function isHappyTicket() {
    let ticket = document.getElementById("ticket-number").value;
    let output = document.getElementById("result");
    let firstTicketHalf = [];
    let lastTicketHalf = [];
    let firstHalfSum = 0;
    let lastHalfSum = 0;

    // Проверяем заполненность поля
    if (ticket === "") {
        output.innerHTML = "<p>Вы не ввели номер билетика ;(</p>"
        return;
    }

    // Проверяем четность кол-ва цифр в поле
    if (ticket.length % 2 !== 0) {
        output.innerHTML = "<p>В билетике нечетное количество цифр!!!</p>"
        return;
    }

    // Заполняем первую половину билетика в массив firstTicketHalf
    for (let i = 0; i < ticket.length / 2; i++) {
        firstTicketHalf.push(ticket[i]);
    }

    // Заполняем вторую половину билетика в другой массив lastTicketHalf
    for (let i = ticket.length / 2; i < ticket.length; i++) {
        lastTicketHalf.push(ticket[i]);
    }

    // Считаем сумму первой половины
    for (let i = 0; i < firstTicketHalf.length; i++) {
        firstHalfSum += parseInt(firstTicketHalf[i]);
    }

    // Считаем сумму воторой половины
    for (let i = 0; i < lastTicketHalf.length; i++) {
        lastHalfSum += parseInt(lastTicketHalf[i]);
    }

    // Сравниваем сумму первой и второй половины цифр, если они равны выдаем что билетик счастливый, иначе - нет
    if (firstHalfSum === lastHalfSum) {
        output.innerHTML = "<p>Билетик счастливый!!! <3</p>"
    }
    else {
        output.innerHTML = "<p>Билетик не счастливый ;(</p>"
    }
}


function styleSwitch() {

    const styleSwitcher = document.getElementById("style-check");
    const docBody = document.getElementById("body-style");

    if (styleSwitcher.checked === true) {
        docBody.setAttribute("class", "dark-mode");
        return;
    }

    docBody.setAttribute("class", "light-mode");
    return;
}